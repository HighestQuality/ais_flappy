//draws the player bird
class Bird {
  float x;
  float y;
  float size = 40;
  float vy = 0;
  float ay = 0.2;
  PImage bird;

  Bird(float initialX, float initialY) {
    x = initialX;
    y = initialY;
    //loads bird image
    bird = loadImage("bird.png");
  }
  
  void setX(float x) {
    this.x = x;
  }
  void setY(float y) {
    this.y = y;
  }

  void draw() {
    pushStyle();

    imageMode(CENTER);
    image(bird, x, y, size, size);

    popStyle();

    y += vy;
    vy += ay;
  }

  void reset() {
    y = 0;
    vy = 0;
  }

}
