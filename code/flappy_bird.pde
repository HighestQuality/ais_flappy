// Group: Marcel Sudau, Johanna Marszalek
//Flappy Bird code comes from https://github.com/sidonath/processing-flappy-bird-clone
//Head recognition as offered in example from Sascha Reinhold
//Extended by two more calibration points

//FaceOSC
import oscP5.*;
OscP5 oscP5;
// Sound Bibliothek
import processing.sound.*;
SoundFile file;


int found;
PVector poseOrientation = new PVector();

PVector birdPoint = new PVector(0, 0);
int birdPointSize = 20;

PVector calibrationVector0 = new PVector(-1, -1);
PVector calibrationVector1 = new PVector(1, 1);
PVector calibrationVector2 = new PVector(1, -1);
PVector calibrationVector3 = new PVector(-1, 1);

Boolean calibrating = true;
int currentCalibrationVector = 0;



Bird bird;
Obstacle[] obstacles = new Obstacle[1];
Score score;
//PImage backgroundImage;
boolean gameStarted = false;
boolean gameOver = false;

void setup() {
    fullScreen(OPENGL);
    
    // Load a soundfile from the /data folder of the sketch and play it in Score class
    file = new SoundFile(this, "blip.wav");
    
    frameRate(180); 
    
    //implements FaceOSC Data
    frameRate = 60;
    oscP5 = new OscP5(this, 8338);
    oscP5.plug(this, "found", "/found");
    oscP5.plug(this, "poseOrientation", "/pose/orientation");
    
    //create game parcours 
    bird = new Bird(width / 2, height / 2);
    obstacles[0] = new Obstacle(width, random(100, height - 100));
    //obstacles[1] = new Obstacle(width * 1.25 + 25, random(100, height - 100));
    score = new Score();
    //backgroundImage = loadImage("bg.png");
}

void draw() {
    //image(backgroundImage, 0, 0, width, height);
    background(0);


    if (calibrating) {
        // draw box
        strokeWeight(4);
        fill(200);
        stroke(63);
        
        if (found != 0) {
            pushMatrix();
            translate(width / 2, height / 2, 0);
            rotateY(poseOrientation.y);
            rotateX(0 - poseOrientation.x);
            rotateZ(poseOrientation.z);

            box(100, 100, 100);
            popMatrix();
        }

        // draw calibration point
        stroke(0, 223, 0);

        if (currentCalibrationVector == 0) { // upper left
            if (frameCount % frameRate < frameRate / 2) { //blinking
                ellipse(birdPointSize, birdPointSize,
                    birdPointSize, birdPointSize);
            }
        } else if (currentCalibrationVector == 1) { // lower right
            if (frameCount % frameRate < frameRate / 2) { //blinking
                ellipse(displayWidth - birdPointSize, displayHeight - birdPointSize,
                    birdPointSize, birdPointSize);
            }
        } else if (currentCalibrationVector == 2) { // upper right
            if (frameCount % frameRate < frameRate / 2) { //blinking
                ellipse(displayWidth - birdPointSize, birdPointSize,
                    birdPointSize, birdPointSize);
            }
        } else if (currentCalibrationVector == 3) { // lower left
            if (frameCount % frameRate < frameRate / 2) { //blinking
                ellipse(birdPointSize, displayHeight - birdPointSize,
                    birdPointSize, birdPointSize);
            }
        }
    } else {
        //as provided in example
        PVector newBirdVector = new PVector(0, 0);
        newBirdVector.x = map(poseOrientation.y, calibrationVector0.y, calibrationVector1.y, 0, displayWidth);
        newBirdVector.y = map(poseOrientation.x, calibrationVector0.x, calibrationVector1.x, 0, displayHeight);

        birdPoint.x = (birdPoint.x * 0.95) + newBirdVector.x * 0.05;
        birdPoint.y = (birdPoint.y * 0.95) + newBirdVector.y * 0.05;

        if (gameOver) {
            drawGameOver();
        } else if (!gameStarted) {
            drawStartScreen();
        } else {
            //bird gets translated Head Data Points
            bird.draw();
            bird.setX(birdPoint.x);
            bird.setY(birdPoint.y);

            for (Obstacle o: obstacles) {
                o.draw();
            }
            score.draw();
            detectCollision();
        }
    }
}

void mousePressed() {
    action();
}
//calibration control
void keyPressed() {
    //FaceOSC
    if (key == 'c') { // if 'c' is pressed
        if (calibrating) {
            // save vector for upper left coordinates
            if (currentCalibrationVector == 0) {
                calibrationVector0.x = poseOrientation.x;
                calibrationVector0.y = poseOrientation.y;
                currentCalibrationVector++;
            }
            // save vector for lower right coordinates
            else if (currentCalibrationVector == 1) {
                calibrationVector1.x = poseOrientation.x;
                calibrationVector1.y = poseOrientation.y;
                //calibrating = false; // end calibration mode
                currentCalibrationVector++;
            }

            // save vector for upper right coordinates
            else if (currentCalibrationVector == 2) {
                calibrationVector2.x = poseOrientation.x;
                calibrationVector2.y = poseOrientation.y;
                currentCalibrationVector++;
            }
            // save vector for lower left coordinates
            else if (currentCalibrationVector == 3) {
                calibrationVector3.x = poseOrientation.x;
                calibrationVector3.y = poseOrientation.y;
                calibrating = false; // end calibration mode
            }

        } else {
            // start calibration mode;
            currentCalibrationVector = 0;
            calibrating = true;
        }
    } else {
        action();
    }
}
//shows if a collision is detected and restarts the routine
void action() {
    if (gameOver) {
        gameOver = false;
        bird.reset();
        for (Obstacle o: obstacles) {
            o.reset();
        }
        score.reset();
    } else if (!gameStarted) {
        gameStarted = true;
    }
}

void drawGameOver() {
    rectMode(CENTER);
    textSize(32);
    textAlign(CENTER, CENTER);
    fill(255);
    text("Game over!",
        width / 2, height / 2,
        width, 100);
}
// menu
void drawStartScreen() {
    rectMode(CENTER);
    textSize(32);
    textAlign(CENTER, CENTER);
    fill(255);
    text("Click to start",
        width / 2, height / 2,
        width, 100);
}

void detectCollision() {
    if (bird.y > height || bird.y < 0) {
        gameOver = true;
    }

    for (Obstacle obstacle: obstacles) {
        if (bird.x - bird.size / 2.0 > obstacle.topX + obstacle.w) {
            score.increase();
        }

        if (obstacle.topX + obstacle.w < 0) {
            obstacle.reposition();
            score.allowScoreIncrease();
        }

        if (obstacle.detectCollision(bird)) {
            gameOver = true;
        }
    }
}
//FaceOSC Events
public void found(int i) {
    found = i;
}

public void poseOrientation(float x, float y, float z) {
    poseOrientation.set(x, y, z);
}
